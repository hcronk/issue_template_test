## :thought_balloon: Summary
_A short summary of your idea for an architecture design change_

## :grimacing: Concerns
### Summary
_The concerns that your design change addresses_

### Conditions
_Under what conditions or at what point could your concerns be realized?_

## :fingers_crossed: Opportunities
### Summary
_The opportunity(ies) that your design change allows us to take advantage of_

### Conditions
_Under what conditions or at what point could your opportunity(ies) be realized?_

## :thinking_face: Implementation ideas
_Any ideas you have for implementing this design change_

## :paperclip: Supporting Information
_Attach or add any diagrams or other supporting information_
<!---Gitlab natively supports inline Mermaid diagrams like the one defined below, 
or you can any attach relevant files to the issue--->

```mermaid
graph TB;
    A[Idea] --> B[Issue];
    B[Create Issue] --> C[Tech Lead Review];
    C[Tech Lead Review] -- Need More Info --> D[1:1 Discussion];
    D[1:1 Discussion] --> E[Refine Issue];
    E[Refine Issue] --> F[Team Discussion];
    C[Tech Lead Review] --> F[Team Discussion];
    C[Tech Lead Review] -- Tech Lead Approval --> G[Create Ticket and Implement];
    F[Team Discussion] -- Tech Lead Approval --> G[Create Ticket and Implement];
    C[Tech Lead Review] -- Tech Lead Veto --> H[Close Issue];
    F[Team Discussion] -- Tech Lead Veto --> H[Close Issue];
```



  

/cc @hcronk